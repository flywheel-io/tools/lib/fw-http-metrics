# fw-http-metrics

FastAPI middleware for collecting and exposing Prometheus metrics.

## Installation

Add as a `poetry` dependency to your project:

```bash
poetry add fw-http-metrics
```

## Usage

```python
from fastapi import FastAPI
from fw_http_metrics import MetricsMiddleware, get_metrics

app = FastAPI()
app.add_middleware(MetricsMiddleware)
app.add_route("/metrics", get_metrics)
```

## Development

Install the project using `poetry` and enable `pre-commit`:

```bash
poetry install
pre-commit install
```

## License

[![MIT](https://img.shields.io/badge/license-MIT-green)](LICENSE)
