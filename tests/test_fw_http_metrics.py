"""Test fw_http_metrics."""

# pylint: disable=redefined-outer-name
import pytest
from fastapi import FastAPI, Response
from fastapi.testclient import TestClient
from prometheus_client.parser import text_string_to_metric_families

from fw_http_metrics import MetricsMiddleware, get_metrics


@pytest.fixture(autouse=True)
def env(monkeypatch, tmp_path):
    """Run tests with 'PROMETHEUS_MULTIPROC_DIR' in the env by default."""
    monkeypatch.setenv("PROMETHEUS_MULTIPROC_DIR", str(tmp_path))


@pytest.fixture
def client():
    """Return a test client for a simple FastAPI instance with metrics."""
    app = FastAPI()
    app.add_middleware(MetricsMiddleware)
    app.add_route("/metrics", get_metrics)
    app.add_route("/foo", lambda _: Response("bar"))
    return TestClient(app)


def test_metrics_keys(client):
    """Test that all expected metric families are exported."""
    metrics = scrape_metrics(client)
    assert set(metrics.keys()) == {
        "fw_request_duration_seconds",
        "fw_requests",
    }


def test_metrics_values(client):
    """Test that GET /foo is recorded via metrics."""
    assert client.get("/foo")
    metrics = scrape_metrics(client)

    request_durations = metrics["fw_request_duration_seconds"]
    assert "method=GET,path=/foo,status=200" in request_durations


def scrape_metrics(client):
    """GET /metrics response and return it parsed for asserting."""
    response = client.get("/metrics")
    metrics = {}
    for metric in text_string_to_metric_families(response.text):
        # skip standard python metrics
        if not metric.name.startswith("fw_"):
            continue
        # "dictify" metrics for simple comparison in tests
        metrics[metric.name] = labels = {}
        for sample in metric.samples:
            label = ",".join(f"{k}={v}" for k, v in sorted(sample.labels.items()))
            labels[label] = sample.value
    return metrics
